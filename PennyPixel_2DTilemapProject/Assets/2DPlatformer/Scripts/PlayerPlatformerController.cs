﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlatformerController : PhysicsObject {

    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;
    private float forceY;
    public Transform shootPoint;
    public GameObject notePrefab;
    private Vector2 notePos;

    Rigidbody2D rb;

    private SpriteRenderer spriteRenderer;
    private Animator animator;
    private HealthBar healthBar;
    

    // Use this for initialization
    void Awake () 
    {
        spriteRenderer = GetComponent<SpriteRenderer> (); 
        animator = GetComponent<Animator> ();
    }

    private void Start()
    {
        healthBar = FindObjectOfType<HealthBar>();
        rb = GetComponent<Rigidbody2D>();
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis ("Horizontal");

        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }

        if (Input.GetButtonDown ("Jump") && grounded) {
            velocity.y = jumpTakeOffSpeed;
        } else if (Input.GetButtonUp ("Jump")) 
        {
            if (velocity.y > 0) {
                velocity.y = velocity.y * 0.5f;
            }
        }

        if(move.x > 0.01f)
        {
            if(spriteRenderer.flipX == true)
            {
                spriteRenderer.flipX = false;
            }
        } 
        else if (move.x < -0.01f)
        {
            if(spriteRenderer.flipX == false)
            {
                spriteRenderer.flipX = true;
            }
        }

        animator.SetBool ("grounded", grounded);
        animator.SetFloat ("velocityX", Mathf.Abs (velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
    }

    public void Damage(float d) {
        healthBar.SetHealth(healthBar.GetHealth() - d);
        if (healthBar.GetHealth() <= 0) {
            Application.LoadLevel(Application.loadedLevel);
        }
    }

    public IEnumerator Knockback(float knockDur) {
        float timer = 0;
        
        while (knockDur > timer)
        {
            timer += Time.deltaTime;
            velocity.y = jumpTakeOffSpeed;
        }

        yield return 0;
    }

    void Shoot() {
        Instantiate(notePrefab, shootPoint.position, shootPoint.rotation);
    }
}

