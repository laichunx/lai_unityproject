﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingLog : MonoBehaviour
{
    float moveSpeed = 3f;
    bool moveRight = true;
    PlayerPlatformerController player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerPlatformerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x >= -6.96f) { moveRight = false; }
        if(transform.position.x <= -10.26f) { moveRight = true; }
        if (moveRight) { transform.position = new Vector2(transform.position.x + moveSpeed * Time.deltaTime, transform.position.y); }
        else { transform.position = new Vector2(transform.position.x - moveSpeed * Time.deltaTime, transform.position.y); }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider) {
            if (moveRight) { player.transform.position = new Vector2(player.transform.position.x + moveSpeed * Time.deltaTime, player.transform.position.y); }
            else { player.transform.position = new Vector2(player.transform.position.x - moveSpeed * Time.deltaTime, player.transform.position.y); }
        }
    }
}
