﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noteShoot : MonoBehaviour
{
    public float speed = 20f;
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb.GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.name);
        if (!collision.CompareTag("sky"))
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        //rb.position = new Vector2(transform.position.x + speed * Time.deltaTime, transform.position.y);
    }
}
